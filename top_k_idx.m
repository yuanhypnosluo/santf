function [top_v_idx] = top_k_idx (v, k)
    sorted_v = unique(v(:));    
    top_v = sorted_v(max(1,end-k+1) : end);
    v_cut = min(top_v(:));
    [vsort, isort] = sort(v, 'descend');
    top_v_idx = isort(vsort >= v_cut);