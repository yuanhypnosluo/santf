function wd_topic_dist(fnA, fnC, fnwds, wd)
% wd, the word you want to find the distribution
    addpath('~/Code/tensor/tensor_toolbox');
    addpath('~/Code/poblano/poblano_toolbox');
    
    astruct = load(fnA);
    A = astruct.A;
    
    cstruct = load(fnC);
    tcore = cstruct.C;

    nt_wd = size(tcore, 3);
    weight = zeros(1, nt_wd);
    for i = 1 : nt_wd
        weight(1,i) = norm(tcore(:,:,i));
    end
    
    spt = load('~/Code/Lisp/late/pt_sg_wd/train.tensor');
    % spt = load('~/Code/Lisp/late/pt_sg_wd/train.doctensor');
    subs = spt(:,1:3);
    vals = spt(:,4);

    wd_idx = unique(subs(:,3));
    
    dt = A{3};
    wm = repmat(weight, size(dt,1), 1); 
    dt = dt.*wm;
    
    % normalize within each topic
    tm = col_norm(dt);
    
    fwds = fopen(fnwds);
    wds = textscan(fwds, '%s', 'Delimiter', '');
    wds = wds{1};

    
    wdid = strmatch(wd, wds);
    wdid = find(ismember(wd_idx,wdid)==1);
    % fprintf('%s id is %d\n', wd, wdid);
    figure;
    bar(sum(tm(wdid,:),1));
    set(gca, 'XLim', [1, size(tm,2)]);
    title(sprintf('%s topic-wise dist', wd));