function res = ntd_eval_sg(fngt, A, tcore)
sg_gt = load(fngt);


sg_gt = full(spconvert(sg_gt));
sg_gt = col_norm(sg_gt);
fprintf('\nSparsity on sg_gt (%d x %d): %f\n', size(sg_gt, 1), size(sg_gt, 2), length(find(sg_gt==0))/(size(sg_gt,1)*size(sg_gt,2)));

% for Tucker tensor, need to reweight by core.
nt_doc = size(tcore,1);
nt_w = size(tcore,3);
nt_sg = size(tcore,2);
weight = zeros(1,nt_sg);
for i = 1:nt_sg
    weight(1,i) = norm(tcore(:,i,:));
end


labs = 1:nt_sg;

dt = A{2};
fprintf('\nSparsity on dt (%d x %d): %f\n', size(dt, 1), size(dt, 2), length(find(dt==0))/(size(dt,1)*size(dt,2)));
wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;
ndt_orig = dt/sum(dt(:));

smooths = 10.^(-7:1:1);
opt_perms = zeros(length(smooths), nt_sg);
opt_kl_divs = zeros(length(smooths), 1);

for iter = 1 : length(smooths)
    smooth = smooths(iter);
    ndt = ndt_orig;
    ndt = col_smooth_norm(ndt, smooth);
    
    % treat the ndt as the discovered topic distribution
    lab_perms = perms(labs);
    psize = size(lab_perms, 1);
    kl_div = zeros(psize, 1);
    for i = 1 : psize
        lab_perm = lab_perms(i, :);
        ndt_perm = ndt(:,lab_perm);
        kl_div(i) = kl_divergence(sg_gt, ndt_perm) / nt_sg;
    end
    
    [min_kl_div, idx] = min(kl_div);
    min_lab_perm = lab_perms(idx,:);
    
    fprintf('[sg_gt res] %.1e (%d, %d, %d) opt perm: [%d', smooth, nt_doc, nt_sg, nt_w, min_lab_perm(1));
    for i = 2:size(min_lab_perm(:))
        fprintf(' %d', min_lab_perm(i));
    end
    fprintf(']; opt kl div: %f\n', min_kl_div);
    opt_perms(iter,:) = min_lab_perm;
    opt_kl_divs(iter) = min_kl_div;
end

res = struct('smooths', smooths, 'opt_perms', opt_perms, 'opt_kl_divs', opt_kl_divs);
