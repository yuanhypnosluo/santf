function top_wdt_for_sgt(fnA, fnC, fnwds, pttid, sgtid, wtnum, wnum)
% pttid - target patient topic
% sgtid - target subgraph topic
% wtnum - number of top word topics you want to see
% wnum - number of top words per topic you want to see.
    addpath('~/Code/tensor/tensor_toolbox');
    addpath('~/Code/poblano/poblano_toolbox');
    
    astruct = load(fnA);
    A = astruct.A;
    
    cstruct = load(fnC);
    tcore = cstruct.C;

    nt_pt = size(tcore, 1);
    nt_sg = size(tcore, 2);
    nt_wd = size(tcore, 3);
    weight = zeros(1, nt_wd);
    for i = 1 : nt_wd
        weight(1,i) = norm(tcore(:,:,i));
    end

    dt = A{3};
    wm = repmat(weight, size(dt,1), 1);
    dt = dt.*wm;
    
    % normalize within each topic
    tm = col_norm(dt);
    
    fwds = fopen(fnwds);
    wds = textscan(fwds, '%s', 'Delimiter', '');
    wds = wds{1};

    
    fprintf('\nsg topic %d\n', sgtid);
    awdt = zeros(nt_wd, 1);
    awdt(:) = tcore(pttid,sgtid,:);
    % awdt = sum(awdt, 1);
    
    top_awdt_idx = top_k_idx(awdt, wtnum);
    
    for i = 1 : length(top_awdt_idx)
        l = top_awdt_idx(i);
        if awdt(l) > 0
            fprintf('word topic %d, (%.2e)\n', l, awdt(l));
            wdt = tm(:,l);
            top_wd_idx = top_k_idx(wdt, wnum);
            for j = 1 : wnum
                m = top_wd_idx(j);
                if wdt(m)>0
                    fprintf('%.3e %s\n', wdt(m), wds{m});
                end
            end
            fprintf('\n');
        end
    end