%finding representative subgraphs of each topic
%topic: # of subgraphs * # of topics
%feature: feature vectors of subgraphs (like words or sentences where they come from)
%n: control the number of random projections  3-4
%scale: the scale to round to integers
function [Rep, Sol,R] = pick_Represent(topic, feature, n)
num = size(topic, 1);              
tp = size(topic, 2); 
[R,k] = LSH(feature, n);          %Locality sensitive hashing
Rep = zeros(k, tp);                      %Representative
Sol = zeros(k,tp);                       %The topic probability value for each subgraph  
for i = 1:tp
    for j = 1 : num
        if topic(j, i) > Sol(R(j),i)           %Sort
            Sol(R(j),i) = topic(j,i);
            Rep(R(j),i) = j;
        end
    end
    [a,b] = sort(Sol(:,i),'descend');           %Reorder the vector such that the bucket with highest prob is put at front
    Rep(:,i) = Rep(b,i);
    Sol(:,i) = Sol(b,i);
end
    


%Locality sensitive hash
function [R,k] = LSH(Data,n)
d = size(Data,2);                      %dimension of data
Proj = randn(d,n);                     
for i = 1 : n
    Proj(:,i) = Proj(:,i)/norm(Proj(:,i));   %normalzie projection vectors
end
H = Data * Proj;                      %projection
a = max(max(abs(H)));
scale = 2/a;                         %heuristic to scale projection matrix 
H = round(H*scale);                   %round to numbers     
[A,X,R] = unique(H,'rows');             %build hash function
k = size(A,1);                          
