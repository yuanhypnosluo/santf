function res = col_norm(m)
rsum = sum(m,1);
res = m ./ repmat(rsum, size(m,1), 1);