function [pre, rec, f] = micro_prf(actual, pred, max_class)
% calculate precision, recall and f-measure given result output
tp = 0; fp = 0; tn = 0; fn = 0;
for class = 1 : max_class
    tp = tp + size(find(actual==class & pred==class), 1);
    fp = fp + size(find(actual~=class & pred==class), 1);
    tn = tn + size(find(actual~=class & pred~=class), 1);
    fn = fn + size(find(actual==class & pred~=class), 1);

end

if(tp+fp == 0) pre = 1; else pre = tp / (tp+fp); end
if(tp+fn == 0) rec = 1; else rec = tp / (tp+fn); end
if(pre+rec == 0) f = 0; else f = 2*pre*rec / (pre+rec); end

end