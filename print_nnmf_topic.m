function print_nnmf_topic(dt, fnstr, N, smooth)


dt = dt/sum(dt(:));
dt = dt + smooth; % smooth
dt = dt/sum(dt(:));

% tc = sum(dt, 1);
% tm = dt ./ repmat(tc, size(dt,1), 1); % normalize each topic

% tc = sum(dt, 2);
% tm = dt ./ repmat(tc, 1, size(dt,2));

fstr = fopen(fnstr);
sgv = textscan(fstr, '%s', 'Delimiter', '');

for i = 1 : size(dt, 2)
    td = dt(:,i);
    [ptop, itop] = sort(td,'descend');
    fprintf('topic %d\n', i);
    for j = 1:N
        fprintf('%f: %s\n', ptop(j), sgv{1}{itop(j)});
    end
end

