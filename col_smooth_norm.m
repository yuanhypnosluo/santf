function res = col_smooth_norm(m, smooth)
res = col_norm(m);
res = res + smooth; % smooth
res = col_norm(res);