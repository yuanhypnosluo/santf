function [b, top] = rank_entropy(topic)
a = sum(topic,2);
j = find(a~=0);
topic(j,:) = topic(j,:)./(a*ones(1,size(topic,2)));
ent = zeros(size(topic,1),1);
for i = 1 : size(topic,1)
    j = find(topic(i,:));
    ent(i) = -sum(topic(i,j).*log(topic(i,j)));
end
[b,top] = sort(ent);