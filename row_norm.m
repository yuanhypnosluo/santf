function res = row_norm(m)
rsum = sum(m,2);
res = m ./ repmat(rsum,1,size(m,2));