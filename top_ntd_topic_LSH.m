function top_ntd_topic_LSH(fnA, fnC, fnsgstr, fnsgft, tnum, lshn, sgnum)
% fnsgft - '~/Code/Lisp/late/pt_sg_wd/sg_train.str'
% tnum - how many top topics per cluster you want to see
% lshn - n used in LSH
% sgnum - how many top subgraphs per topics you want to see

    addpath('~/Code/tensor/tensor_toolbox');
    addpath('~/Code/poblano/poblano_toolbox');

    sg_ft = dlmread(fnsgft, ' ');

    astruct = load(fnA);
    A = astruct.A;

    % for Tucker tensor, need to reweight by core.
    cstruct = load(fnC);
    tcore = cstruct.C;
    nt_pt = size(tcore, 1);
    nt_sg = size(tcore, 2);
    nt_wd = size(tcore, 3);
    weight = zeros(1, nt_sg);
    for i = 1 : nt_sg
        weight(1,i) = norm(tcore(:,i,:));
    end



    dt = A{2};
    wm = repmat(weight, size(dt,1), 1);
    dt = dt.*wm;

    dt = dt/sum(dt(:));

    % normalize within each topic
    tc = sum(dt, 1);
    tm = dt ./ repmat(tc, size(dt,1), 1); 

    fsgstr = fopen(fnsgstr);
    sgv = textscan(fsgstr, '%s', 'Delimiter', '');

    sg_ft = full(spconvert(sg_ft));
    for i = 1 : size(sg_ft,1)
        sg_ft(i,:) = sg_ft(i,:)/norm(sg_ft(i,:));
    end
    [Rep, Sol, R] = pick_Represent(tm, sg_ft, lshn);

    fprintf('\nHashed into %d buckets\n', size(Rep,1));

    for k = 1 : nt_pt
        fprintf('\n\ncluster %d\n', k);
        mpt = zeros(nt_sg, nt_wd);
        mpt(:,:) = tcore(k,:,:);
        mpt = sum(mpt, 2);
        sorted_mpt = unique(mpt(:));
        top_mpt = sorted_mpt(end-tnum+1 : end);
        mpt_cut = min(top_mpt(:));
        top_mpt_idx = find(mpt >= mpt_cut);
        for i = 1 : size(top_mpt_idx,1)
            l = top_mpt_idx(i);
            fprintf('%d topic %d\n', i, l);
            for j = 1 : min(size(Rep,1), sgnum)
                if Rep(j,l) > 0 % in case bucket number > topic size
                    fprintf('%.2e: %s\n', Sol(j,l), sgv{1}{Rep(j,l)});
                end
            end
            fprintf('\n');
        end
    end

