function [] = plot_threshold_res(fnres)
% plot means with 0.05 confidence interval.

a = load(fnres);
res = a.res;
N = res.N; p = res.p; r = res.r; f1 = res.f1;

x = 1:N+1; x = (x-1)/N; figure; hold on;

alpha = 0.05;
u_alpha = normcdf(1-alpha/2);
iters = res.maxiter;

p_m = mean(p,1); p_s = std(p,0,1);
errorbar(x, p_m, p_s*u_alpha/sqrt(iters), 'r');

r_m = mean(r,1); r_s = std(r,0,1);
errorbar(x, r_m, r_s*u_alpha/sqrt(iters), 'g');

f1_m = mean(f1,1); f1_s = std(f1,0,1);
errorbar(x, f1_m, f1_s*u_alpha/sqrt(iters), 'b');

hleg = legend('precision', 'recall', 'f-measure');
set(hleg, 'location', 'Best');
xlim([0 1]); ylim([0.2 1]);
title('performance evaluation (mean with \alpha=0.05 confidence interval)');
xlabel('percentage threshold T (set clustering label as 1 if p>=T)')