function nsdm = pt_sgg_gen(A, tcore)
    nt_pt = size(tcore,1);
    nt_w = size(tcore,3);
    nt_sg = size(tcore,2);
    weight = zeros(1,nt_pt);
    dt = A{1};
    for i = 1:nt_pt
        weight(1,i) = norm(tcore(i,:,:));
    end
    
    wm = repmat(weight, size(dt,1), 1);
    dt = dt.*wm;
    nsdm = dt/sum(dt(:));
