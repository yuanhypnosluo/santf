%X is the test matrix, W is the factor matrix learned from 
% training data
%V is the factor matrix for test documents
function H = nnmf_test(X, W)
[m,n] = size(X);
[a,b] = size(W);
k = m;
if (a == m)
    X = X';
    k = n;
end
iter = 0;
maxiter = 40;
cond = 1;
H = rand(k,b);
while(cond)
    H_old = H;
    iter = iter + 1;
    A = X * W;
    B = H * W' * W;
    j = find(B);
    C = 0 * A;
    C(j) = A(j)./B(j);
    H = H.*C;
    if norm(H_old-H)/norm(H) < 0.01 ||iter == maxiter
        cond = 0;
    end
   % norm(X - H * W');
end
    
    
