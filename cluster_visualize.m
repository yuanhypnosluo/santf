function [] = cluster_visualize(res, fign)
% assume inst_gt is multi-class, single label
    inst_ft = res.ft; inst_names = res.inst_names; 
    inst_gt = res.gt; class_names = res.class_names;
    inst_clusters = res.cluster;
              
    figure;
    colormap('hot');
    % imagesc(inst_ft);
    % colorbar;

    pt_class = zeros(size(inst_names));
    csizes = zeros(length(class_names),1);

    inst_ft_order = []; 
    % append class labels according to ground truth
    for i = 1:length(class_names)
        cinds = find(inst_gt(:,i)==1);
        for j = 1:length(cinds)
            inst_names(cinds(j)) = strcat(inst_names(cinds(j)), '_', class_names(i));
        end
    end    
    
    for i = 1:length(class_names)
        inst_ft_g = inst_ft(inst_clusters(:,i)==1,:);
        inst_names_g = inst_names(inst_clusters(:,i)==1,:);
        csizes(i) = size(inst_ft_g,1);
        if isempty(inst_ft_order)
            inst_ft_order = inst_ft_g;
            inst_names_order = inst_names_g;
        else
            inst_ft_order = [inst_ft_order; inst_ft_g];
            inst_names_order = [inst_names_order; inst_names_g];
        end
    end
    
    % inst_ft_order = tfidf(inst_ft_order);
    % inst_ft_order = log10(inst_ft_order+1);
    inst_ft_order = inst_ft_order > 0;
    
    imagesc(inst_ft_order);
    colorbar;
    set(gca, 'YTickLabel', inst_names_order);
    set(gca, 'YTick', 1:length(inst_names_order));
    set( gca, 'TickDir', 'out' );
    strt = sprintf('%s - ', fign);
    for i = 1:length(class_names)
        strta = sprintf('%s: %d ', class_names{i}, csizes(i));
        strt = strcat(strt, strta);
    end
    
    title(strt);