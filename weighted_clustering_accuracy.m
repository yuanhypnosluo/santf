function [res] = weighted_clustering_accuracy(gt,mc)

addpath('~/Code/matlab_util');
nc = size(mc,2);
mc = row_norm(mc);

cm = conf_mat(gt, mc); 



[mapre, marec, maf] = macro_prf_conf_mat(cm);
[mipre, mirec, mif] = micro_prf_conf_mat(cm);
acc = weighted_acc(gt,mc);

res = struct('cm', cm, 'mapre', mapre, 'marec', marec, 'maf', maf, 'mipre', mipre, 'mirec', mirec, 'mif', mif, 'acc', acc);

