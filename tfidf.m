function res = tfidf(mat)
ndoc = size(mat, 1);
idf = log2(ndoc ./ sum(mat > 0, 1));
idf(isinf(idf)) = 0;
res = log2(mat+1) .* repmat(idf, ndoc, 1);