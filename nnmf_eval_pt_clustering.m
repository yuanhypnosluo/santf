function res = nnmf_eval_pt_clustering(pt_gt, nsdm)
addpath('~/Code/matlab_util');
nt_pt = size(nsdm,2);
labs = 1:nt_pt;

matrix_sparsity(nsdm, 'nsdm');
matrix_sparsity(pt_gt, 'pt_gt');



% treat the nsdm as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);
mapre = 0; marec = 0; maf = 0; mipre = 0; mirec = 0; mif = 0; acc = 0;
cluster = zeros(size(pt_gt));

for i = 1 : psize
    lab_perm = lab_perms(i, :);
    nsdm_perm = nsdm(:,lab_perm);
    eval = clustering_accuracy(pt_gt, nsdm_perm);
    if maf < eval.maf
        mapre = eval.mapre; marec = eval.marec; maf = eval.maf;
        mipre = eval.mipre; mirec = eval.mirec; mif = eval.mif;
        cm = eval.cm; max_lab_perm = lab_perm; acc = eval.acc;
        cluster = eval.cluster;
    end
end


fprintf('\n[pt_gt res] opt lab perm: [%d', max_lab_perm(1));
for i = 2:size(max_lab_perm(:))
    fprintf(' %d', max_lab_perm(i));
end

fprintf(']\n'); 
cfm_eval_disp(cm);

fprintf('acc: %.3f, mapre: %.3f, marec: %.3f, opt maf: %.3f, ', ...
    acc, mapre, marec, maf);
fprintf('mipre: %.3f, mirec: %.3f, mif: %.3f\n', ...
    mipre, mirec, mif);

res = struct('opt_perm', max_lab_perm, 'opt_mapre', mapre, ...
             'opt_marec', marec, 'opt_maf', maf, 'opt_mipre', mipre, ...
             'opt_mirec', mirec, 'opt_mif', mif, 'opt_acc', acc, ...
             'opt_cluster', cluster);