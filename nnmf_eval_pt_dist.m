function res = nnmf_eval_pt_dist(fngt, nsdm)

pt_gt = load(fngt); 



fprintf('\nSparsity on nsdm (%d x %d): %f\n', size(nsdm, 1), size(nsdm, 2), length(find(nsdm==0))/(size(nsdm,1)*size(nsdm,2)));

fprintf('\nSparsity on pt_gt (%d x %d): %f\n', size(pt_gt, 1), size(pt_gt, 2), length(find(pt_gt==0))/(size(pt_gt,1)*size(pt_gt,2)));



% treat the nsdm as the discovered topic distribution



res = cluster_mort_count(pt_gt, nsdm);
