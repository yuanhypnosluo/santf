function res = ntd_eval_pt_clustering(pt_gt, A, tcore, pts)
addpath('~/Code/matlab_util');

% for Tucker tensor, need to reweight by core.
nt_pt = size(tcore,1);
nt_w = size(tcore,3);
nt_sg = size(tcore,2);
weight = zeros(1,nt_pt);
dt = A{1};

for i = 1:nt_pt
    weight(1,i) = norm(tcore(i,:,:));
end

wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;
nsdm = dt/sum(dt(:));


labs = 1:nt_pt;

matrix_sparsity(nsdm, 'nsdm');
matrix_sparsity(pt_gt, 'pt_gt');



% treat the nsdm as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);
acc = 0; mapre = acc; marec = acc; maf = acc; 
mipre = acc; mirec = acc; mif = acc;

% to use kmeans, added 12132013
%nsdm = l2_row_norm(nsdm);
%[nsdm, c] = kmeans(nsdm, size(pt_gt,2), 'emptyaction', 'singleton');
%nsdm = multi_col_gt(nsdm);

for i = 1 : psize
    lab_perm = lab_perms(i, :);
    nsdm_perm = nsdm(:,lab_perm);
    eval = clustering_accuracy(pt_gt, nsdm_perm);
    if maf < eval.maf
        mapre = eval.mapre; marec = eval.marec; maf = eval.maf;
        mipre = eval.mipre; mirec = eval.mirec; mif = eval.mif;
        cm = eval.cm; max_lab_perm = lab_perm; acc = eval.acc; cfs = eval.cfs;
        cluster = eval.cluster;
    end
end



fprintf('[pt_gt res] (%d, %d, %d) opt lab perm: [%d', ...
        nt_pt, nt_sg, nt_w, max_lab_perm(1));
for i = 2:size(max_lab_perm(:))
    fprintf(' %d', max_lab_perm(i));
end


fprintf(']\n');
cfm_eval_disp(cm);
cfs_disp(cfs, pts);

fprintf('acc: %.3f, mapre: %.3f, marec: %.3f, opt maf: %.3f, ', ...
    acc, mapre, marec, maf);
fprintf('mipre: %.3f, mirec: %.3f, mif: %.3f\n', ...
    mipre, mirec, mif);

res = struct('opt_perm', max_lab_perm, 'opt_cm', cm, 'opt_acc', acc, ...
             'opt_mapre', mapre, 'opt_marec', marec, 'opt_maf', maf, ...
             'opt_mipre', mipre, 'opt_mirec', mirec, 'opt_mif', mif, ...
             'opt_cluster', cluster);