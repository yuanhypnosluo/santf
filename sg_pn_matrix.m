function [res] = sg_pn_matrix(nt_sg, nc)
load '~/Code/Lisp/late/sg_pn_matrix_train';
load '~/Code/Lisp/late/sg_subset_idx';

sg_pn = full(spconvert(sg_pn_matrix_train));

tic;
% sdm -subgraph dimension matrix
sdm = nnmf(sg_pn, nt_sg);
fprintf('NNMF ');
toc;


tc = sum(sdm, 2);
tcm = repmat(tc, 1, size(sdm,2));
nsdm = sdm./tcm; % normalized subgraph dimension matrix


idx = kmeans(nsdm, nc, 'emptyaction', 'singleton');
tidx = idx(sg_subset_idx);
fnout = sprintf('nnmf_result/cluster_ids_s%d_c%d.txt', nt_sg, nc);
dlmwrite(fnout, tidx, 'delimiter', ',');

load selected_subgraph_cluster_ids.txt

[tpre, trec, tf1, tspec] = measure(tidx, selected_subgraph_cluster_ids);
fprintf('\n\nNNMF (nt_sg %d; nc %d) - pre: %f; rec: %f; f1: %f; spec: %f\n', nt_sg, nc, tpre, trec, tf1, tspec);

res=struct('tpre', tpre, 'trec', trec, 'tf1', tf1);