function [res] = threshold_clustering_accuracy(gt,mc, N)

addpath('~/Code/matlab_util');
mc = row_norm(mc);



p = zeros(N+1,1); r = zeros(N+1,1); f1 = zeros(N+1,1);


for i = 1:N+1
    [p(i), r(i), f1(i)] = threshold_prf(gt, mc, (i-1)/N); % 
    acc = weighted_acc(gt, mc);
end


% fprintf('mapre: %.3f, marec: %.3f, maf: %.3f, mipre: %.3f, mirec: %.3f, mif: %.3f\n', mapre, marec, maf, mipre, mirec, mif);
res = struct('p', p, 'r', r, 'f1', f1);

