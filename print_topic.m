function print_topic(fntensor, fnsgstr, N, smooth)

addpath('~/Code/tensor/tensor_toolbox');
addpath('~/Code/poblano/poblano_toolbox');
tensor_struct = load(fntensor);
T = tensor_struct.T;

% for Tucker tensor, need to reweight by core.
tcore = T.core;
nt_sg = size(tcore,2);
weight = zeros(1, nt_sg);
for i = 1 : nt_sg
    weight(1,i) = norm(tcore(:,i,:));
end

dt = T.U{2};
wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;

dt = dt/sum(dt(:));
dt = dt + smooth; % smooth
dt = dt/sum(dt(:));

% tc = sum(dt, 1);
% tm = dt ./ repmat(tc, size(dt,1), 1); % normalize each topic

tc = sum(dt, 2);
tm = dt ./ repmat(tc, 1, size(dt,2));

fsgstr = fopen(fnsgstr);
sgv = textscan(fsgstr, '%s', 'Delimiter', '');

for i = 1 : size(tm, 2)
    td = tm(:,i);
    [ptop, itop] = sort(td,'descend');
    fprintf('topic %d\n', i);
    for j = 1:N
        fprintf('%f: %s\n', ptop(j), sgv{1}{itop(j)});
    end
end

