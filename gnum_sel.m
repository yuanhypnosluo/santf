function [] = gnum_sel(fn, xstart, ystart, xend, yend);
maf = csvread(fn);
maf = maf(:,2:4);

% $$$ idx = maf(:,1)<200;
% $$$ maf = maf(idx,:);
% $$$ idx = maf(:,2)<200;
% $$$ maf = maf(idx,:);

if nargin >= 2
    idx = maf(:,1) >= xstart;
    maf = maf(idx,:);
end

if nargin >= 3
    idx = maf(:,2) >= ystart;
    maf = maf(idx, :);
end

if nargin >= 4
    idx = maf(:,1) <= xend;
    maf = maf(idx, :);
end

if nargin >= 5
    idx = maf(:,2) <= yend;
    maf = maf(idx, :);
end


xtick_lab = sort(unique(maf(:,2)));
xtick = xtick_lab;

ytick_lab = sort(unique(maf(:,1)));
ytick = ytick_lab;

maf = full(spconvert(maf));
max_maf = max(maf(:));
[ymax, xmax] = find(maf==max_maf);
[x, y] = meshgrid(xtick, ytick);
[xx yy] = meshgrid(xtick(1):xtick(end), ytick(1):ytick(end));
maf = interp2(x, y, maf(ytick, xtick), xx, yy);

% fprintf('max_maf %.2f, x %d, y %d\n', max_maf, xmax, ymax);

xstart = xstart - 1;
ystart = ystart - 1;
xtick = xtick - xstart;
ytick = ytick - ystart;

figure;
set(gcf, 'Position', [100, 100, 420, 420]);
% $$$ subplot(1,2,1);
% $$$ surf(maf);
% $$$ set(gca, 'XTick', xtick);
% $$$ set(gca, 'YTick', ytick);
% $$$ set(gca, 'XLim', [xtick(1) xtick(end)]);
% $$$ set(gca, 'YLim', [ytick(1) ytick(end)]);
% $$$ set(gca, 'XTickLabel', xtick_lab);
% $$$ set(gca, 'YTickLabel', ytick_lab);
% $$$ xlabel('word group number');
% $$$ ylabel('subgraph group number');
% $$$ zlabel('average f-measure');
% $$$ 
% $$$ subplot(1,2,2);
clb = floor(100 * min(maf(:)))/100;
cub = ceil(100 * max(maf(:)))/100;
contour(maf, 'ShowText', 'On'); hold on; % clb:0.02:cub, 
xmax = xmax - xstart; ymax = ymax - ystart;
plot(xmax, ymax, 'r*', 'MarkerSize', 8);
annotation = sprintf('\\leftarrow max avg f: %.3f', max_maf);
text(xmax, ymax, annotation, 'Color','black','FontWeight','bold');

if 0
    xtick = xtick(mod(xtick,20)==0);
    xtick_lab = xtick_lab(mod(xtick_lab,20)==0);
    
    ytick = ytick(mod(ytick,20)==0);
    ytick_lab = ytick_lab(mod(ytick_lab,20)==0);
end

set(gca, 'XTick', xtick);
set(gca, 'YTick', ytick);

% $$$ if nargin < 4
% $$$     xend = xtick(end);
% $$$ else
% $$$     xtick = xtick(xtick<=xend);
% $$$     xtick_lab = xtick_lab(xtick_lab<=xend);
% $$$ end
% $$$ if nargin < 5
% $$$     yend = ytick(end);
% $$$ else
% $$$     ytick = ytick(ytick<=yend);
% $$$     ytick_lab = ytick_lab(ytick_lab<=yend);
% $$$ end

set(gca, 'XTickLabel', xtick_lab);
set(gca, 'YTickLabel', ytick_lab);
set(gca, 'XLim', [xtick(1) xtick(end)]);
set(gca, 'YLim', [ytick(1) ytick(end)]);

xlabel('word group number');
ylabel('subgraph group number');