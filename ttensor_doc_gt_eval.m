function res = ttensor_doc_gt_eval(T)
doc_gt = dlmread('~/Code/Lisp/late/matlab_data/lymphoma_train_label_doc_spmat', ',');


doc_gt = full(spconvert(doc_gt));
doc_gt = doc_gt';
doc_gt = doc_gt/sum(doc_gt(:));
nt_doc = size(doc_gt,2);
labs = 1:nt_doc;
% for Tucker tensor, need to reweight by core.
tcore = T.core;
weight = zeros(1,nt_doc);
dt = T.U{1};

for i = 1:nt_doc
    weight(1,i) = norm(tcore(i,:,:));
end


fprintf('\nSparsity on dt (%d x %d): %f\n', size(dt, 1), size(dt, 2), length(find(dt==0))/(size(dt,1)*size(dt,2)));
wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;
ndt = dt/sum(dt(:));

% treat the ndt as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);
kl_div = zeros(psize, 1);
for i = 1 : psize
    lab_perm = lab_perms(i, :);
    ndt_perm = ndt(:,lab_perm);
    kl_div(i) = js_divergence(doc_gt, ndt_perm);
end

[min_js_div, idx] = min(kl_div);
min_lab_perm = lab_perms(idx,:);

fprintf('[doc_gt res] (%d, %d, %d, %f) opt perm: [%d', nt_doc, nt_sg, nt_w, ratio, min_lab_perm(1));
for i = 2:size(min_lab_perm(:))
    fprintf(' %d', min_lab_perm(i));
end
fprintf(']; opt js div: %f\n', min_js_div);



res = struct('opt_perm', min_lab_perm, 'opt_js_div', min_js_div);