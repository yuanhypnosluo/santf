function [subs, vals] = sparse_slice(subs, vals, idx, d)
    idx = sort(idx);
    sp_idx = find(ismember(subs(:,d),idx));
    vals = vals(sp_idx,:);
    subs = subs(sp_idx,:);
    sub(:,d) = find(idx==sub(:,d));
    