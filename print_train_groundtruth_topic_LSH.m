function print_train_groundtruth_topic_LSH(fntopic, fnsgstr, fnsgft, n)
% fntopic - '~/Code/Lisp/late/lymphoma_train_label_subgraph_spmat'
% fnsgstr - '~/Code/Lisp/late/lymphoma_train_sg_str_list'
% fnsgft - '~/Code/Lisp/late/lymphoma_train_sg_pn_matrix'
% n - 3, 4 or 5 etc.


sg_ft = dlmread(fnsgft, ' ');

tm = load(fntopic);
tm = full(spconvert(tm));
tm = tm';

% normalize within each topic
tc = sum(tm, 1);
tm = tm ./ repmat(tc, size(tm,1), 1); 

fsgstr = fopen(fnsgstr);
sgv = textscan(fsgstr, '%s', 'Delimiter', '');

sg_ft = full(spconvert(sg_ft));
for i = 1 : size(sg_ft,1)
    sg_ft(i,:) = sg_ft(i,:)/norm(sg_ft(i,:));
end
[Rep, Sol] = pick_Represent(tm, sg_ft, n);

fprintf('\nHashed into %d buckets\n', size(Rep,1));
for i = 1 : size(Rep, 2)
    fprintf('topic %d\n', i);
    for j = 1:size(Rep,1)
        if Rep(j,i) ~= 0
            fprintf('%f: %s\n', Sol(j,i), sgv{1}{Rep(j,i)});
        end
    end
end
