function res = ttensor_sg_gt_eval(T)
sg_gt = dlmread('~/Code/Lisp/late/matlab_data/lymphoma_train_label_subgraph_spmat', ',');


sg_gt = full(spconvert(sg_gt));
sg_gt = sg_gt';
sg_gt = sg_gt/sum(sg_gt(:));
labs = 1:size(sg_gt,2);
% for Tucker tensor, need to reweight by core.
tcore = T.core;
weight = zeros(1,nt_sg);
for i = 1:nt_sg
    weight(1,i) = norm(tcore(:,i,:));
end

dt = T.U{2};
fprintf('\nSparsity on dt (%d x %d): %f\n', size(dt, 1), size(dt, 2), length(find(dt==0))/(size(dt,1)*size(dt,2)));
wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;
ndt = dt/sum(dt(:));


% treat the ndt as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);
kl_div = zeros(psize, 1);
for i = 1 : psize
    lab_perm = lab_perms(i, :);
    ndt_perm = ndt(:,lab_perm);
    kl_div(i) = js_divergence(sg_gt, ndt_perm);
end

[min_kl_div, idx] = min(kl_div);
min_lab_perm = lab_perms(idx,:);

fprintf('[sg_gt res] (%d, %d, %d, %f) opt perm: [%d', nt_doc, nt_sg, nt_w, ratio, min_lab_perm(1));
for i = 2:size(min_lab_perm(:))
    fprintf(' %d', min_lab_perm(i));
end
fprintf(']; opt kl div: %f\n', min_kl_div);


res = struct('opt_perm', min_lab_perm, 'opt_kl_div', min_kl_div);
