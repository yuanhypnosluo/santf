function entropy_rank_filtered_topic(fntensor, fnsgstr, N, smooth)

addpath('~/Code/tensor/tensor_toolbox');
addpath('~/Code/poblano/poblano_toolbox');
tensor_struct = load(fntensor);
T = tensor_struct.T;

% for Tucker tensor, need to reweight by core.
tcore = T.core;
nt_sg = size(tcore,2);
weight = zeros(1, nt_sg);
for i = 1 : nt_sg
    weight(1,i) = norm(tcore(:,i,:));
end

dt = T.U{2};
wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;

dt = dt/sum(dt(:));
dt = dt + smooth; % smooth
dt = dt/sum(dt(:));

fsgstr = fopen(fnsgstr);
sgv = textscan(fsgstr, '%s', 'Delimiter', '');

dt_sum = sum(dt,2);


[dt_sum_sort, dt_rank] = sort(dt_sum, 'descend');


dt_sort_N = dt(dt_rank(1:N),:);
dt_rank_N = dt_rank(1:N);
[ent_esort, dt_erank] = rank_entropy(dt_sort_N);

% [ent_esort, dt_erank] = rank_entropy(dt);
% ent_esort_N = ent_esort(1:N);
% dt_erank_N = dt_erank(1:N);





% for j = 1 : 4
%     [dt_sort, dt_rank] = sort(dt(dt_erank_N,j), 'descend');
%     fprintf('topic %d\n', j);
%     for i = 1 : N 
%         fprintf('%f %s\n', ent_sort_esort(i), sgv{1}{dt_rank_N(dt_erank(i))}); 
%         fprintf('%f %s\n', dt_sort(i), sgv{1}{dt_erank_N(dt_rank(i))}); 
%     end
% end


for i = 1 : N 
    fprintf('%f %s\n', ent_esort(i), sgv{1}{dt_rank_N(dt_erank(i))}); 
end