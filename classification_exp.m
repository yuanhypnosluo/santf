function res = classification_exp(threshold, fnT_train, smooth)
% T denotes the factorized tensor
addpath('~/Code/tensor/tensor_toolbox');
addpath('~/Code/poblano/poblano_toolbox');

load './data/lymphoma_train_pat_doc'; % ~/Code/Lisp/late
load './data/lymphoma_test_pat_doc';
load './data/lymphoma_test_tensor';

lstruct = load(fnT_train);
T_train = lstruct.T;

train_doc_ft = T_train.U{1};
train_doc_ft = train_doc_ft + smooth; % smooth
train_core = T_train.core;
nt_doc = size(train_core,1);
train_doc_wt = zeros(1,nt_doc);
for i = 1:nt_doc
    train_doc_wt(1,i) = norm(train_core(i,:,:));
end
train_doc_wt_mat = repmat(train_doc_wt, size(train_doc_ft,1), 1);
train_doc_ft = train_doc_ft.*train_doc_wt_mat;

subs = lymphoma_test_tensor(:,1:3);
vals = lymphoma_test_tensor(:,4);
T_test_raw = sptensor(subs,vals,[4586 size(T_train,2) size(T_train,3)]);
size(T_test_raw)
tic;
size(T_train)
T_test = tucker_als_test(T_test_raw, size(T_train.core), T_train, 1);
fprintf('matching training tensor into testing data ');
toc;

test_doc_ft = T_test.U{1};
test_doc_ft = test_doc_ft + smooth; % smooth
test_core = T_test.core;
nt_doc = size(test_core,1);
test_doc_wt = zeros(1,nt_doc);
for i = 1:nt_doc
    test_doc_wt(1,i) = norm(test_core(i,:,:));
end
test_doc_wt_mat = repmat(test_doc_wt, size(test_doc_ft,1), 1);
test_doc_ft = test_doc_ft.*test_doc_wt_mat;



load './data/burkitts_train_pat_sit';
load './data/burkitts_test_pat_sit';

burkitts_stats = classify_doc_feature(burkitts_train_pat_sit, lymphoma_train_pat_doc, train_doc_ft, burkitts_test_pat_sit, lymphoma_test_pat_doc, test_doc_ft, threshold);
fprintf('burkitts: pre: %.3f, rec: %.3f, f1: %.3f\n', burkitts_stats.pre, burkitts_stats.rec, burkitts_stats.f1);


load '~/Code/Lisp/late/dlbcl_train_pat_sit'
load '~/Code/Lisp/late/dlbcl_test_pat_sit'
dlbcl_stats = classify_doc_feature(dlbcl_train_pat_sit, lymphoma_train_pat_doc, train_doc_ft, dlbcl_test_pat_sit, lymphoma_test_pat_doc, test_doc_ft, threshold);
fprintf('dlbcl: pre: %.3f, rec: %.3f, f1: %.3f\n', dlbcl_stats.pre, dlbcl_stats.rec, dlbcl_stats.f1);


load '~/Code/Lisp/late/follicular_train_pat_sit'
load '~/Code/Lisp/late/follicular_test_pat_sit'
follicular_stats = classify_doc_feature(follicular_train_pat_sit, lymphoma_train_pat_doc, train_doc_ft, follicular_test_pat_sit, lymphoma_test_pat_doc, test_doc_ft, threshold);
fprintf('follicular: pre: %.3f, rec: %.3f, f1: %.3f\n', follicular_stats.pre, follicular_stats.rec, follicular_stats.f1);


load '~/Code/Lisp/late/hodgkins_train_pat_sit'
load '~/Code/Lisp/late/hodgkins_test_pat_sit'
hodgkins_stats = classify_doc_feature(hodgkins_train_pat_sit, lymphoma_train_pat_doc, train_doc_ft, hodgkins_test_pat_sit, lymphoma_test_pat_doc, test_doc_ft, threshold);
fprintf('hodgkins: pre: %.3f, rec: %.3f, f1: %.3f\n', hodgkins_stats.pre, hodgkins_stats.rec, hodgkins_stats.f1);

res = ( burkitts_stats.f1 + dlbcl_stats.f1 + follicular_stats.f1 + hodgkins_stats.f1 ) / 4;