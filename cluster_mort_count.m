function res = cluster_mort_count(gt,m)
gt(gt<0) = 0; % need to fix ground truth
mmax = max(m,[],2);
mmax(mmax==0) = 1;
m = m ./ repmat(mmax, 1, size(m,2));
m(m<1) = 0;
fprintf('cluster sizes: ');
msum = sum(m,1);
for i=1:length(msum)
    fprintf('%d ', msum(i));
end
nc = size(m,2);
mmean = zeros(1,nc);
for i = 1:nc
    mmean(i) = mean(gt( m(:,i)==1, 2 ));
end

[msort, mix] = sort(mmean);
fprintf('sorted mean: ');
for i = 1:nc-1
    fprintf('%d, ', msort(i));
end
fprintf('%d\n', msort(nc));

res = cell(nc,1);
for i = 1:nc
    res{i} = gt( m(:,mix(i))==1, 2 ); % just include those mortality days within this cluster
end