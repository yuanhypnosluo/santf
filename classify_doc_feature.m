%classify patients based on their documents and document feature
%pat_sit: matrix #of patients * 4
%pat_doc: 2 columns array, [patient_id, document_id]
%doc_feature: row i is the feature vectors for document i, tensor
%association matrix weighted by core
%test_pat_sit, test_pat_doc, tset_doc_feature: same as above for test data
%res: # of test patients * 4
%acc: 1 for correct, 0 for mistake comparing to test_pat_sit
%threshold: is the thresold for classify if a pat has lyphoma i, the res is
%in continous value, so threshold will make the results binary. should be between (0,1)
%precision: precision for each of the 4 classifiers
%recall, specificity, F1: same here
%avg_res: average results for all 4 classifier, in the form [precision,%recall, spcificity, F1]

function stats = classify_doc_feature(pat_sit, pat_doc, doc_feature, test_pat_sit, test_pat_doc, test_doc_feature, threshold)
nd = size(pat_sit,2);
doc_res = zeros(size(test_doc_feature,1),nd);
for  i = 1 : nd
	   Group = zeros(size(doc_feature, 1),1);
j = find(pat_sit(:,i) == 1);
isj = ismember(pat_doc(:,1),j);
Group(pat_doc(isj,2)) = 1;                               %each document has an label from weather its correspoding patient has lyphoma i
svmstruct = svmtrain(doc_feature, Group);
size(doc_feature)
size(test_doc_feature)

doc_res(:,i) = svmclassify(svmstruct,test_doc_feature);  %1 or 0 predicitons for each test document for different lyphoma i
end
np = size(test_pat_sit,1);
num_doc = zeros(np,1);
res = zeros(np,nd);
for i = 1 : size(test_pat_doc,1)
	  res(test_pat_doc(i,1),:) = res(test_pat_doc(i,1),:) + doc_res(test_pat_doc(i,2),:);          %averaging the predictions from documents fro each test patient
	  num_doc(test_pat_doc(i,1)) = num_doc(test_pat_doc(i,1)) + 1;
end
for i = 1 : np
	  res(i,:) = res(i,:) / num_doc(i);
end
[precision,recall, specificity, F1, avg_res] = measure(res, test_pat_sit, threshold);
stats = struct('pre', precision, 'rec', recall, 'spec', specificity, 'f1', F1, 'avg_res', avg_res, 'res', res);

%comnpute the accuracy matrix, ideally the more 1s the better.


function [precision,recall, specificity, F1, avg_res] = measure(res, test_pat_sit, threshold)
res = (res > threshold);
nd = size(res,2);
precision = zeros(1,nd);                             %precision for each of the 4 classifiers
recall = zeros(1,nd);                                %recall ......
specificity = zeros(1,nd);                           %specificity ...
F1 = zeros(1,nd);                                    %F1...
avg_res = zeros(1,nd);                               %averaging results for all 4 classifiers
for i = 1 : nd
	  tp = sum(res(:,i).*test_pat_sit(:,i));
fp = sum(res(:,i).*(~test_pat_sit(:,i)));
fn = sum((~res(:,i)).*(test_pat_sit(:,i)));
tn = sum((~res(:,i)).*(~test_pat_sit(:,i)));
precision(i) = tp/(tp + fp);
recall(i) = tp/(tp + fn);
specificity(i) = fp/(fp+tn);
F1(i) = 2 * precision(i) * recall(i) / (precision(i) + recall(i));
end
tp = sum(sum(res.*test_pat_sit));
fp = sum(sum(res.*(~test_pat_sit)));
fn = sum(sum((~res).*(test_pat_sit)));
tn = sum(sum((~res).*(~test_pat_sit)));
avg_res(1) = tp/(tp + fp);
avg_res(2)= tp/(tp + fn);
avg_res(3) = fp/(fp+tn);
avg_res(4) = 2 * avg_res(1) * avg_res(2) / (avg_res(1) + avg_res(2));



