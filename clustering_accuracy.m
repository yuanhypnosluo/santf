function [res] = clustering_accuracy(gt,m)

[mmax, midx] = max(m,[],2);
mc = zeros(size(m));
for i = 1 : length(midx)
   mc(i,midx(i)) = 1;
end



cm = conf_mat(gt, mc); 
cfs = zeros(sum(cm(:))-trace(cm), 3);
nc = size(cm,1);
p = 0;
for i = 1:nc
    for j = 1:nc
        if i ~= j
            bsz = cm(i,j); % batch size
            bt = find(gt(:,i)==1 & mc(:,j)==1); % misclustered batch
            cfs(p+1:p+bsz,:) = [i*ones(bsz,1), j*ones(bsz,1), bt];
            p = p+bsz;
        end
    end
end

[mapre, marec, maf] = macro_prf_conf_mat(cm);
[mipre, mirec, mif] = micro_prf_conf_mat(cm);
acc = weighted_acc(gt,mc);

res = struct('cm', cm, 'mapre', mapre, 'marec', marec, 'maf', maf, ...
             'mipre', mipre, 'mirec', mirec, 'mif', mif, 'acc', ...
             acc, 'cluster', mc, 'cfs', cfs);

