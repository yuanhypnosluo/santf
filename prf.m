function [pre, rec, f] = prf(actual, pred, class)
% calculate precision, recall and f-measure given result output
tp = size(find(actual==class & pred==class), 1);
fp = size(find(actual~=class & pred==class), 1);
tn = size(find(actual~=class & pred~=class), 1);
fn = size(find(actual==class & pred~=class), 1);
if(tp+fp == 0) pre = 1; else pre = tp / (tp+fp); end
if(tp+fn == 0) rec = 1; else rec = tp / (tp+fn); end
if(pre+rec == 0) f = 0; else f = 2*pre*rec / (pre+rec); end
end