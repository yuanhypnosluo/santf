function [rgt, rdata] = cluster_balance(gt, data)
    gtcs = sum(gt);
    gtcs
    cs_max = max(gtcs);
    rid = 0;
    rdata = zeros(sum(round(cs_max ./ gtcs)), size(data,2));
    for i = 1:size(gt,2)
        multiplier = round(cs_max / gtcs(i));
        rr1 = rid+1;
        rr2 = gtcs(i)*multiplier+rid;
        fprintf('cluster %d: %d - %d\n', i, rr1, rr2);
        rd1 = 1+sum(gtcs(1:i))-gtcs(i);
        rd2 = sum(gtcs(1:i));
        rdata(rr1:rr2,:) = repmat(data(rd1:rd2,:), multiplier, 1);
        rgt(rr1:rr2,:) = repmat(gt(rd1:rd2,:), multiplier, 1);
        rid = gtcs(i)*multiplier+rid;
    end
    