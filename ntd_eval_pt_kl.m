function res = ntd_eval_pt_kl(fngt, A, tcore)

gt = load(fngt); 

pt_gt = full(spconvert(gt));

% for Tucker tensor, need to reweight by core.
nt_pt = size(tcore,1);
nt_w = size(tcore,3);
nt_sg = size(tcore,2);
npt = size(pt_gt,1);
weight = zeros(1,nt_pt);
dt = A{1};

for i = 1:nt_pt
    weight(1,i) = norm(tcore(i,:,:));
end

wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;
nsdm = dt/sum(dt(:));


labs = 1:nt_pt;


fprintf('\nSparsity on nsdm (%d x %d): %f\n', size(nsdm, 1), size(nsdm, 2), length(find(nsdm==0))/(size(nsdm,1)*size(nsdm,2)));

fprintf('\nSparsity on pt_gt (%d x %d): %f\n', size(pt_gt, 1), size(pt_gt, 2), length(find(pt_gt==0))/(size(pt_gt,1)*size(pt_gt,2)));



% treat the nsdm as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);
kls = zeros(npt, psize);
for i = 1 : psize
    lab_perm = lab_perms(i, :);
    nsdm_perm = nsdm(:,lab_perm);
    for j = 1 : npt
        if (max(nsdm_perm(j,:)) == 0)
            nsdm_perm(j,:) = nsdm_perm(j,:) + 1;
        end
        kls(j,i) = js_divergence(pt_gt(j,:), nsdm_perm(j,:));
        if isinf(kls(j,i))
            fprintf('kl inf!\n');
        end
    end
end
mean_kls = mean(kls, 1);
[mean_kl, idx] = min(mean_kls);
opt_lab_perm = lab_perms(idx,:);
kl = kls(:,idx);

fprintf('\n[pt_gt res] (%d, %d, %d) opt lab perm: [%d', nt_pt, nt_sg, nt_w, opt_lab_perm(1));
for i = 2:size(opt_lab_perm(:))
    fprintf(' %d', opt_lab_perm(i));
end
fprintf(']; mean_kl: %.3f\n', mean_kl)

res = struct('mean_kl', mean_kl, 'kl', kl);

