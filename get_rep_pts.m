addpath('~/Code/tensor/tensor_toolbox');
addpath('~/Code/poblano/poblano_toolbox');
addpath('~/Code/matlab_util');

rep_thr = 0.8;

a = load('/data/mghcfl/lymph_ntd/pt_tr_te_10262013/ntd_A_4_180_50.mat');
A = a.A;

c = load('/data/mghcfl/lymph_ntd/pt_tr_te_10262013/ntd_C_4_180_50.mat');
C = c.C;

weight = zeros(1,4);
for i = 1:4
    weight(1,i) = norm(C(i,:,:));
end

dt= A{1};
wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;
nsdm = dt/sum(dt(:));

nsdm = row_norm(nsdm);

fngt = '~/Code/Lisp/late/pt_sg_wd/pt_gt_train.spmat';
gt = load(fngt); 
pt_gt = full(spconvert(gt));

pt_idx = find(sum(pt_gt,2)==1);
pt_gt = pt_gt(pt_idx,:);

gtsz = length(pt_idx);
train_size = 509; % floor(gtsz * train_ratio);
pt_tr_idx = zeros(gtsz, 1); 
pt_te_idx = ones(gtsz,1);
pt_tr_idx(1:train_size,:) = pt_tr_idx(1:train_size,:) + 1; 
pt_te_idx(1:train_size,:) = pt_te_idx(1:train_size,:) - 1;

pt_gt_tr = pt_gt(pt_tr_idx==1,:);
pt_gt_te = pt_gt(pt_te_idx==1,:);
nsdm_tr = nsdm(pt_tr_idx==1,:);
nsdm_te = nsdm(pt_te_idx==1,:);

max_nsdm_tr = max(nsdm_tr, [], 2);
max_nsdm_te = max(nsdm_te, [], 2);

tr_idx = max_nsdm_tr >= rep_thr;
te_idx = max_nsdm_te >= rep_thr;

fprintf('rep_thr is %.2f\n', rep_thr);
fprintf('training rep size: %d\n', sum(tr_idx));
fprintf('testing rep size: %d\n', sum(te_idx));

lab_perm = [1 4 3 2];
res = clustering_accuracy(pt_gt_tr(tr_idx,:), nsdm_tr(tr_idx, lab_perm)); 
tr_acc = res.acc;
tr_mapre = res.mapre; tr_marec = res.marec; 
tr_maf = res.maf; tr_mif = res.mif;
tr_mipre = res.mipre; tr_mirec = res.mirec; 

cfm_eval_disp(res.cm);

res = clustering_accuracy(pt_gt_te(te_idx,:), nsdm_te(te_idx, lab_perm)); 
te_acc = res.acc;
te_mapre = res.mapre; te_marec = res.marec; 
te_maf = res.maf; te_mif = res.mif;
te_mipre = res.mipre; te_mirec = res.mirec; 

cfm_eval_disp(res.cm);

fprintf('\ntraining acc: %.3f; ', tr_acc);
fprintf('mapre: %.3f; marec: %.3f; maf: %.3f; ', tr_mapre, tr_marec, tr_maf);
fprintf('mipre: %.3f; mirec: %.3f; mif: %.3f\n', tr_mipre, tr_mirec, tr_mif);

fprintf('\ntesting acc: %.3f; ', te_acc);
fprintf('mapre: %.3f; marec: %.3f; maf: %.3f; ', te_mapre, te_marec, te_maf);
fprintf('mipre: %.3f; mirec: %.3f; mif: %.3f\n', te_mipre, te_mirec, te_mif);
