function print_ntd_topic_LSH(fnA, fnC, fnsgstr, fnsgft, n)
% fnsgft - '~/Code/Lisp/late/pt_sg_wd/sg_train.str'

% load sentence feature
% load './data/sg_lg_matrix_train';
% load word feature
addpath('~/Code/tensor/tensor_toolbox');
addpath('~/Code/poblano/poblano_toolbox');

sg_ft = dlmread(fnsgft, ' ');

astruct = load(fnA);
A = astruct.A;

% for Tucker tensor, need to reweight by core.
cstruct = load(fnC);
tcore = cstruct.C;
nt_sg = size(tcore,2);
weight = zeros(1, nt_sg);
for i = 1 : nt_sg
    weight(1,i) = norm(tcore(:,i,:));
end

dt = A{2};
wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;

dt = dt/sum(dt(:));

% normalize within each topic
tc = sum(dt, 1);
tm = dt ./ repmat(tc, size(dt,1), 1); 

fsgstr = fopen(fnsgstr);
sgv = textscan(fsgstr, '%s', 'Delimiter', '');

sg_ft = full(spconvert(sg_ft));
for i = 1 : size(sg_ft,1)
    sg_ft(i,:) = sg_ft(i,:)/norm(sg_ft(i,:));
end
[Rep, Sol, R] = pick_Represent(tm, sg_ft, n);

fprintf('\nHashed into %d buckets\n', size(Rep,1));
for i = 1 : size(Rep, 2)
    fprintf('topic %d\n', i);
    for j = 1:size(Rep,1)
        if Rep(j,i) > 0 % in case bucket number > topic size
            fprintf('%.2e: %s\n', Sol(j,i), sgv{1}{Rep(j,i)});
        end
    end
end

for i = 1 : size(Rep,1)
    fprintf('\nbucket %d\n', i);
    bids = find(R==i);
    for j = 1 : length(bids)
        fprintf('%s\n', sgv{1}{bids(j)});
    end
end