function div = kl_divergence(p,q)
assert(min(size(p)==size(q)));
pv = p(:) / sum(p(:));
qv = q(:) / sum(q(:));
idx = find(pv);
a = log2(pv(idx)./qv(idx)).*pv(idx);
% a(isinf(a))=0;
div = sum(a(:));