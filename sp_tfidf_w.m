function res = sp_tfidf_w(subs, vals)
%% treat (iw) as a unit
ndoc = max(subs(:,1));
nsg = max(subs(:,2));
nw = max(subs(:,3));
res = vals;
idf = zeros(nsg, nw);
adocs = cell(nsg, nw);

tic;
for i = 1:size(subs,1)
    isg = subs(i,2);
    iw = subs(i,3);
    adocs{iw} = cat(1, adocs{iw}, subs(i,1));
end
fprintf('done with first scanning ');
toc;
tic;
for i = 1:size(subs,1)
    isg = subs(i,2);
    iw = subs(i,3);
    if idf(isg, iw) == 0
        idf(isg, iw) = log2(ndoc ./ length(unique(adocs{iw})) );
    end
end

fprintf('done with idf ');
toc;
for i = 1:size(subs,1)
    isg = subs(i,2);
    iw = subs(i,3);
    res(i) = log2(vals(i)+1) .* idf(isg, iw);
end