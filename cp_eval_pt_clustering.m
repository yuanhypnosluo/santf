function res = cp_eval_pt_clustering(pt_gt, A, lambda)
addpath('~/Code/matlab_util');

% for Tucker tensor, need to reweight by core.
nt_pt = size(A{1},2);
weight = lambda';
dt = A{1};


wm = repmat(weight, size(dt,1), 1);
% dt = dt.*wm;
nsdm = dt; %/sum(dt(:));


labs = 1:size(pt_gt,2);

matrix_sparsity(nsdm, 'nsdm');
matrix_sparsity(pt_gt, 'pt_gt');



% treat the nsdm as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);
acc = 0; mapre = acc; marec = acc; maf = acc; 
mipre = acc; mirec = acc; mif = acc;

% to use kmeans, added 12132013
nsdm = l2_row_norm(nsdm);
[nsdm, c] = kmeans(nsdm, size(pt_gt,2), 'emptyaction', 'singleton');
nsdm = multi_col_gt(nsdm);

for i = 1 : psize
    lab_perm = lab_perms(i, :);
    nsdm_perm = nsdm(:,lab_perm);
    eval = clustering_accuracy(pt_gt, nsdm_perm);
    if maf < eval.maf
        mapre = eval.mapre; marec = eval.marec; maf = eval.maf;
        mipre = eval.mipre; mirec = eval.mirec; mif = eval.mif;
        cm = eval.cm; max_lab_perm = lab_perm; acc = eval.acc;
    end
end



fprintf('[pt_gt res] (%d) opt lab perm: [%d', nt_pt, max_lab_perm(1));
for i = 2:size(max_lab_perm(:))
    fprintf(' %d', max_lab_perm(i));
end


fprintf(']\n');
cfm_eval_disp(cm);

fprintf('acc: %.3f, mapre: %.3f, marec: %.3f, opt maf: %.3f, ', ...
    acc, mapre, marec, maf);
fprintf('mipre: %.3f, mirec: %.3f, mif: %.3f\n', ...
    mipre, mirec, mif);

res = struct('opt_perm', max_lab_perm, 'opt_cm', cm, 'opt_acc', acc, ...
    'opt_mapre', mapre, 'opt_marec', marec, 'opt_maf', maf, ...
    'opt_mipre', mipre, 'opt_mirec', mirec, 'opt_mif', mif);