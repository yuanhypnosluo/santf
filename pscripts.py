""" yluo - 04/30/2013 creation
"""
__author__= """Yuan Luo (yuanluo@mit.edu)"""
__revision__="0.5"

#! /usr/bin/python;
import os
import subprocess

def mimic_nnst_parallel_script(qtype="big"):
    dnjobs = "/PHShome/yl960/Code/jobs/lsf_scripts"
    dnscripts = dnjobs + "/pmimic_nnst"
    dntensors = "/PHShome/yl960/Code/kdd_2013/mimic_nnst"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
        
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    subprocess.call("rm -rf "+dntensors+"/*", shell=True)
    workdir = "/PHShome/yl960/Code/kdd_2013"
    cnt = 0
    # write .lsf scripts 100, 150, 200, 250, 300, 350, 400, 500, 600, 700, 800
    for nt_pt in [3, 10, 20, 30, 40, 50, 60]: # 
        nt_sg = 3
        for nt_w in [3, 10, 20, 30, 40, 50, 60]: # 6, 8, 
            # ratio = 0
            for ratio in [0]: # , 0.6, 0.7 , 0.1, 0.2, 0.3, 0.4, 0.5
                cnt += 1        
                fnnst_tmp = open("pmimic_nnst_tmp.lsf", "r")
                fnnst = open("%s/pmimic_nnst_%s.lsf" % (dnscripts, cnt), "w")
                while 1:
                    ln = fnnst_tmp.readline()
                    if not ln:
                        break
                    ln = ln.rstrip(" \n")
                    if (ln == "[setting]"):
                        fnnst.write("workdir=%s\n" % (workdir))
                        fnnst.write("nt_pt=%d\n" % (nt_pt))
                        fnnst.write("nt_sg=%d\n" % (nt_sg))
                        fnnst.write("nt_w=%d\n" % (nt_w))
                        fnnst.write("ratio=%s\n" % (ratio))
                    else:
                        fnnst.write(ln+"\n")
                fnnst_tmp.close()
                fnnst.close()
        

    # write .sh script
    fsh_tmp = open("pmimic_nnst_tmp.sh", "r")
    fsh = open(dnjobs+"/pmimic_nnst.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/pmimic_nnst.sh", 
                    shell=True)
    return;


def mimic_ntd_parallel_script(qtype="big", homedir="/PHShome/yl960"):
    dnjobs = "%s/Code/jobs/lsf_scripts" % (homedir)
    dnscripts = dnjobs + "/pmimic_ntd"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
        
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    workdir = "%s/Code/kdd_2013" % (homedir)
    cnt = 0
    # write .lsf scripts 
    for nt_pt in [3, 10, 20, 30, 40, 50]:
        nt_sg = 3
        for nt_w in [3, 6, 10, 20, 30, 40, 50]:
            cnt += 1        
            fntd_tmp = open("pmimic_ntd_tmp.lsf", "r")
            fntd = open("%s/pmimic_ntd_%s.lsf" % (dnscripts, cnt), "w")
            while 1:
                ln = fntd_tmp.readline()
                if not ln:
                    break
                ln = ln.rstrip(" \n")
                if (ln == "[setting]"):
                    fntd.write("workdir=%s\n" % (workdir))
                    fntd.write("nt_pt=%d\n" % (nt_pt))
                    fntd.write("nt_sg=%d\n" % (nt_sg))
                    fntd.write("nt_w=%d\n" % (nt_w))
                else:
                    fntd.write(ln+"\n")
            fntd_tmp.close()
            fntd.close()
        

    # write .sh script
    fsh_tmp = open("pmimic_ntd_tmp.sh", "r")
    fsh = open(dnjobs+"/pmimic_ntd.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/pmimic_ntd.sh", 
                    shell=True)
    return;

def mimic_comorb_ntd_pt_parallel_script(qtype="big"):
    dnjobs = "/PHShome/yl960/Code/jobs/lsf_scripts"
    dnscripts = dnjobs + "/pmimic_comorb_ntd_pt"
    dntensors = "/PHShome/yl960/Code/kdd_2013/mimic_comorb_ntd_pt"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
        
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    subprocess.call("rm -rf "+dntensors+"/*", shell=True)
    workdir = "/PHShome/yl960/Code/kdd_2013"
    cnt = 0
    # write .lsf scripts 
    for nt_sg in [2, 10, 20, 30, 40, 50]:
        nt_pt = 2
        for nt_w in [2, 6, 10, 20, 30, 40, 50]:
            cnt += 1        
            fntd_tmp = open("pmimic_comorb_ntd_pt_tmp.lsf", "r")
            fntd = open("%s/pmimic_comorb_ntd_pt_%s.lsf" % (dnscripts, cnt), "w")
            while 1:
                ln = fntd_tmp.readline()
                if not ln:
                    break
                ln = ln.rstrip(" \n")
                if (ln == "[setting]"):
                    fntd.write("workdir=%s\n" % (workdir))
                    fntd.write("nt_pt=%d\n" % (nt_pt))
                    fntd.write("nt_sg=%d\n" % (nt_sg))
                    fntd.write("nt_w=%d\n" % (nt_w))
                else:
                    fntd.write(ln+"\n")
            fntd_tmp.close()
            fntd.close()
        

    # write .sh script
    fsh_tmp = open("pmimic_comorb_ntd_pt_tmp.sh", "r")
    fsh = open(dnjobs+"/pmimic_comorb_ntd_pt.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/pmimic_comorb_ntd_pt.sh", 
                    shell=True)
    return;

def mimic_ntd_pt_parallel_script(qtype="medium"):
    dnjobs = "/PHShome/yl960/Code/jobs/lsf_scripts"
    dnscripts = dnjobs + "/pmimic_ntd_pt"
    dntensors = "/PHShome/yl960/Code/kdd_2013/mimic_ntd_pt"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
        
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    subprocess.call("rm -rf "+dntensors+"/*", shell=True)
    workdir = "/PHShome/yl960/Code/kdd_2013"
    cnt = 0
    # write .lsf scripts 
    for nt_sg in [2, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140]:
        nt_pt = 2
        for nt_w in [2, 4, 6, 8, 10, 20]: # 6, 10, 20, 30, 40, 50
            cnt += 1        
            fntd_tmp = open("pmimic_ntd_pt_tmp.lsf", "r")
            fntd = open("%s/pmimic_ntd_pt_%s.lsf" % (dnscripts, cnt), "w")
            while 1:
                ln = fntd_tmp.readline()
                if not ln:
                    break
                ln = ln.rstrip(" \n")
                if (ln == "[setting]"):
                    fntd.write("workdir=%s\n" % (workdir))
                    fntd.write("nt_pt=%d\n" % (nt_pt))
                    fntd.write("nt_sg=%d\n" % (nt_sg))
                    fntd.write("nt_w=%d\n" % (nt_w))
                else:
                    fntd.write(ln+"\n")
            fntd_tmp.close()
            fntd.close()
        

    # write .sh script
    fsh_tmp = open("pmimic_ntd_pt_tmp.sh", "r")
    fsh = open(dnjobs+"/pmimic_ntd_pt.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/pmimic_ntd_pt.sh", 
                    shell=True)
    return;


def mimic_ntd_pt_tr_te_parallel_script(qtype="medium", homedir='/PHShome/yl960', cdn='/data/mghcfl/mimic', maxiter=15):
    dnjobs = "%s/Code/jobs/lsf_scripts" % (homedir)
    dnscripts = dnjobs + "/pmimic_ntd_pt_tr_te"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
   
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    workdir = "%s/Code/kdd_2013" % (homedir)
    cnt = 0
    # write .lsf scripts 
    for nt_sg in [2, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140]:
        nt_pt = 2
        for nt_w in [2, 4, 6, 8, 10, 20]: # 6, 10, 20, 30, 40, 50
            cnt += 1        
            fntd_tmp = open("pmimic_ntd_pt_tr_te_tmp.lsf", "r")
            fntd = open("%s/pmimic_ntd_pt_tr_te_%s.lsf" % (dnscripts, cnt), "w")
            while 1:
                ln = fntd_tmp.readline()
                if not ln:
                    break
                ln = ln.rstrip(" \n")
                if (ln == "[setting]"):
                    fntd.write("workdir=%s\n" % (workdir))
                    fntd.write("nt_pt=%d\n" % (nt_pt))
                    fntd.write("nt_sg=%d\n" % (nt_sg))
                    fntd.write("nt_w=%d\n" % (nt_w))
                    fntd.write("maxiter=%d\n" % (maxiter))
                    fntd.write("cdn=%s\n" % (cdn))
                else:
                    fntd.write(ln+"\n")
            fntd_tmp.close()
            fntd.close()
        

    # write .sh script
    fsh_tmp = open("pmimic_ntd_pt_tr_te_tmp.sh", "r")
    fsh = open(dnjobs+"/pmimic_ntd_pt_tr_te.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/pmimic_ntd_pt_tr_te.sh", 
                    shell=True)
    return;

def lymph_ntd_parallel_script(qtype="big"):
    dnjobs = "/PHShome/yl960/Code/jobs/lsf_scripts"
    dnscripts = dnjobs + "/plymph_ntd"
    dntensors = "/PHShome/yl960/Code/kdd_2013/lymph_ntd"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
        
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    subprocess.call("rm -rf "+dntensors+"/*", shell=True)
    workdir = "/PHShome/yl960/Code/kdd_2013"
    cnt = 0
    # write .lsf scripts 
    for nt_pt in [4, 10, 20, 30, 40, 50]:
        nt_sg = 4
        for nt_w in [4, 6, 10, 20, 30, 40, 50]:
            cnt += 1        
            fntd_tmp = open("plymph_ntd_tmp.lsf", "r")
            fntd = open("%s/plymph_ntd_%s.lsf" % (dnscripts, cnt), "w")
            while 1:
                ln = fntd_tmp.readline()
                if not ln:
                    break
                ln = ln.rstrip(" \n")
                if (ln == "[setting]"):
                    fntd.write("workdir=%s\n" % (workdir))
                    fntd.write("nt_pt=%d\n" % (nt_pt))
                    fntd.write("nt_sg=%d\n" % (nt_sg))
                    fntd.write("nt_w=%d\n" % (nt_w))
                else:
                    fntd.write(ln+"\n")
            fntd_tmp.close()
            fntd.close()
        

    # write .sh script
    fsh_tmp = open("plymph_ntd_tmp.sh", "r")
    fsh = open(dnjobs+"/plymph_ntd.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/plymph_ntd.sh", 
                    shell=True)
    return;


def lymph_ntd_doc_tr_parallel_script(qtype="big"):
    dnjobs = "/PHShome/yl960/Code/jobs/lsf_scripts"
    dnscripts = dnjobs + "/plymph_ntd_doc_tr"
    dntensors = "/PHShome/yl960/Code/kdd_2013/lymph_ntd_doc_tr"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
        
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    subprocess.call("rm -rf "+dntensors+"/*", shell=True)
    workdir = "/PHShome/yl960/Code/kdd_2013"
    cnt = 0
    # write .lsf scripts 
    for nt_sg in [4, 10, 20, 30, 40, 50]:
        nt_pt = 4
        for nt_w in [4, 6, 10, 20, 30, 40, 50]:
            cnt += 1        
            fntd_tmp = open("plymph_ntd_doc_tr_tmp.lsf", "r")
            fntd = open("%s/plymph_ntd_doc_tr_%s.lsf" % (dnscripts, cnt), "w")
            while 1:
                ln = fntd_tmp.readline()
                if not ln:
                    break
                ln = ln.rstrip(" \n")
                if (ln == "[setting]"):
                    fntd.write("workdir=%s\n" % (workdir))
                    fntd.write("nt_pt=%d\n" % (nt_pt))
                    fntd.write("nt_sg=%d\n" % (nt_sg))
                    fntd.write("nt_w=%d\n" % (nt_w))
                else:
                    fntd.write(ln+"\n")
            fntd_tmp.close()
            fntd.close()
        

    # write .sh script
    fsh_tmp = open("plymph_ntd_doc_tr_tmp.sh", "r")
    fsh = open(dnjobs+"/plymph_ntd_doc_tr.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/plymph_ntd_doc_tr.sh", 
                    shell=True)
    return;


def lymph_ntd_pt_tr_parallel_script(qtype="medium"):
    dnjobs = "/PHShome/yl960/Code/jobs/lsf_scripts"
    dnscripts = dnjobs + "/plymph_ntd_pt_tr"
    dntensors = "/data/mghcfl/lymph_ntd/pt_tr"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
        
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    subprocess.call("rm -rf "+dntensors+"/*", shell=True)
    workdir = "/PHShome/yl960/Code/kdd_2013"
    cnt = 0
    # write .lsf scripts 
    for nt_sg in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180, 200]:
        for nt_pt in [2, 4]: # 4, 
            for nt_w in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180, 200]:
                cnt += 1        
                fntd_tmp = open("plymph_ntd_pt_tr_tmp.lsf", "r")
                fntd = open("%s/plymph_ntd_pt_tr_%s.lsf" % (dnscripts, cnt), "w")
                while 1:
                    ln = fntd_tmp.readline()
                    if not ln:
                        break
                    ln = ln.rstrip(" \n")
                    if (ln == "[setting]"):
                        fntd.write("workdir=%s\n" % (workdir))
                        fntd.write("nt_pt=%d\n" % (nt_pt))
                        fntd.write("nt_sg=%d\n" % (nt_sg))
                        fntd.write("nt_w=%d\n" % (nt_w))
                    else:
                        fntd.write(ln+"\n")
                fntd_tmp.close()
                fntd.close()
        

    # write .sh script
    fsh_tmp = open("plymph_ntd_pt_tr_tmp.sh", "r")
    fsh = open(dnjobs+"/plymph_ntd_pt_tr.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/plymph_ntd_pt_tr.sh", 
                    shell=True)
    return;


def lymph_ntd_pt_tr_te_parallel_script(qtype="medium"):
    dnjobs = "/PHShome/yl960/Code/jobs/lsf_scripts"
    dnscripts = dnjobs + "/plymph_ntd_pt_tr_te"
    dntensors = "/data/mghcfl/lymph_ntd/pt_tr_te"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
    if not os.path.isdir(dntensors):
        os.makedirs(dntensors)
        
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    subprocess.call("rm -rf "+dntensors+"/*", shell=True)
    workdir = "/PHShome/yl960/Code/kdd_2013"
    cnt = 0
    # write .lsf scripts 10, 20, 200
    for nt_sg in [30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180]:
        for nt_pt in [3]: #   10, 20, 200
            for nt_w in [30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180]:
                cnt += 1        
                fntd_tmp = open("plymph_ntd_pt_tr_te_tmp.lsf", "r")
                fntd = open("%s/plymph_ntd_pt_tr_te_%s.lsf" % (dnscripts, cnt), "w")
                while 1:
                    ln = fntd_tmp.readline()
                    if not ln:
                        break
                    ln = ln.rstrip(" \n")
                    if (ln == "[setting]"):
                        fntd.write("workdir=%s\n" % (workdir))
                        fntd.write("nt_pt=%d\n" % (nt_pt))
                        fntd.write("nt_sg=%d\n" % (nt_sg))
                        fntd.write("nt_w=%d\n" % (nt_w))
                    else:
                        fntd.write(ln+"\n")
                fntd_tmp.close()
                fntd.close()
        

    # write .sh script
    fsh_tmp = open("plymph_ntd_pt_tr_te_tmp.sh", "r")
    fsh = open(dnjobs+"/plymph_ntd_pt_tr_te.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/plymph_ntd_pt_tr_te.sh", 
                    shell=True)
    return;

def lymph_ntd_pt_tr_kl_parallel_script(qtype="medium"):
    dnjobs = "/PHShome/yl960/Code/jobs/lsf_scripts"
    dnscripts = dnjobs + "/plymph_ntd_pt_tr_kl"
    dntensors = "/data/mghcfl/lymph_ntd/pt_tr_kl"
    if not os.path.isdir(dnscripts):
        os.makedirs(dnscripts)
        
    # clear the .lsf scripts dir
    subprocess.call("rm "+dnscripts+"/*", shell=True)
    subprocess.call("rm -rf "+dntensors+"/*", shell=True)
    workdir = "/PHShome/yl960/Code/kdd_2013"
    cnt = 0
    # write .lsf scripts 
    for nt_sg in [4, 10, 20, 30, 40, 50]:
        nt_pt = 4
        for nt_w in [4, 6, 10, 20, 30, 40, 50]:
            cnt += 1        
            fntd_tmp = open("plymph_ntd_pt_tr_kl_tmp.lsf", "r")
            fntd = open("%s/plymph_ntd_pt_tr_kl_%s.lsf" % (dnscripts, cnt), "w")
            while 1:
                ln = fntd_tmp.readline()
                if not ln:
                    break
                ln = ln.rstrip(" \n")
                if (ln == "[setting]"):
                    fntd.write("workdir=%s\n" % (workdir))
                    fntd.write("nt_pt=%d\n" % (nt_pt))
                    fntd.write("nt_sg=%d\n" % (nt_sg))
                    fntd.write("nt_w=%d\n" % (nt_w))
                else:
                    fntd.write(ln+"\n")
            fntd_tmp.close()
            fntd.close()
        

    # write .sh script
    fsh_tmp = open("plymph_ntd_pt_tr_kl_tmp.sh", "r")
    fsh = open(dnjobs+"/plymph_ntd_pt_tr_kl.sh", "w")
    while 1:
        ln = fsh_tmp.readline()
        if not ln:
            break
        ln = ln.rstrip(" \n")
        if (ln == "[setting]"):
            fsh.write("nscripts=%s\n" % (cnt))
            fsh.write("qtype="+qtype+"\n")
        else:
            fsh.write(ln+"\n")
    fsh_tmp.close()
    fsh.close()

    subprocess.call("chmod a+x ~/Code/jobs/lsf_scripts/plymph_ntd_pt_tr_kl.sh", 
                    shell=True)
    return;
