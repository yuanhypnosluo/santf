sg_pn = dlmread('./sg_pn.spmat', ' '); % dtstat.dat;
% number of clusters
nc = 50;
% selecting the document and topic columns
dt = full(spconvert(sg_pn));

tc = sum(dt, 2);
tcm = repmat(tc, 1, size(dt, 2));

% normalize so that it forms a probability distribution
ndt = dt./tcm;

% figure; hold on;
% for i=1:size(ndt,1)
%     plot(ndt(i,:), 'k-');
% end
[idx, c] = kmeans(ndt, nc, 'emptyaction', 'singleton');

dlmwrite('cluster_idx.txt', idx, 'delimiter', ',');