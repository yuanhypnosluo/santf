function res = sp_tfidf(subs, vals)
%% treat isg as unit
ndoc = max(subs(:,1));
nsg = max(subs(:,2));
res = vals;
idf = zeros(nsg, 1);
adocs = cell(nsg, 1);

tic;
for i = 1:size(subs,1)
    isg = subs(i,2);
    adocs{isg} = cat(1, adocs{isg}, subs(i,1));
end
fprintf('done with first scanning ');
toc;
tic;
for i = 1:size(subs,1)
    isg = subs(i,2);
    if idf(isg) == 0
        idf(isg) = log2(ndoc ./ length(unique(adocs{isg})) );
    end
end

fprintf('done with idf ');
toc;
for i = 1:size(subs,1)
    isg = subs(i,2);
    res(i) = log2(vals(i)+1) .* idf(isg);
end