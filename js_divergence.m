function jsd = js_divergence(p,q)
avg = (p + q) / 2;
jsd = .5 * kl_divergence(p, avg) + .5 * kl_divergence(q, avg);