function res = nnmf_eval_pt_threshold_clustering(fngt, nsdm, N)
addpath('~/Code/matlab_util');
gt = load(fngt); 
nt_pt = size(nsdm,2);
labs = 1:nt_pt;
pt_gt = full(spconvert(gt));


matrix_sparsity(nsdm, 'nsdm');
matrix_sparsity(pt_gt, 'pt_gt');


% treat the nsdm as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);
max_f1 = 0;


for i = 1 : psize
    lab_perm = lab_perms(i, :);
    nsdm_perm = nsdm(:,lab_perm);
    eval = threshold_clustering_accuracy(pt_gt, nsdm_perm, N);
    if max_f1 < max(eval.f1)
        p = eval.p; r = eval.r; f1 = eval.f1;
        max_lab_perm = lab_perm; max_f1 = max(eval.f1);
    end
end


fprintf('\n[pt_gt res] opt lab perm: [%d', max_lab_perm(1));
for i = 2:size(max_lab_perm(:))
    fprintf(' %d', max_lab_perm(i));
end

fprintf(']\n'); 





res = struct('opt_perm', max_lab_perm, 'opt_p', p, ...
    'opt_r', r, 'opt_f1', f1);