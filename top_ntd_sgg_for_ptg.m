function top_ntd_sgg_for_ptg(fnA, fnC, fnsgstr, tnum, tpts, sgnum)
% returns the top sg group for a specific (e.g., misclassified) pt group
% fnsgft - '~/Code/Lisp/late/pt_sg_wd/sg_train.str'
% tnum - how many top topics per cluster you want to see
% sgnum - how many top subgraphs per topics you want to see
% tpts - pt group under investigation, name only 

    addpath('~/Code/tensor/tensor_toolbox');
    addpath('~/Code/poblano/poblano_toolbox');

    sg_ft = dlmread(fnsgft, ' ');

    astruct = load(fnA);
    A = astruct.A;

    % for Tucker tensor, need to reweight by core.
    cstruct = load(fnC);
    tcore = cstruct.C;
    nt_pt = size(tcore, 1);
    nt_sg = size(tcore, 2);
    nt_wd = size(tcore, 3);
    weight = zeros(1, nt_sg);
    for i = 1 : nt_sg
        weight(1,i) = norm(tcore(:,i,:));
    end



    dt = A{2};
    wm = repmat(weight, size(dt,1), 1);
    dt = dt.*wm;

    dt = dt/sum(dt(:));

    % normalize within each topic
    tc = sum(dt, 1);
    tm = dt ./ repmat(tc, size(dt,1), 1); 

    fsgstr = fopen(fnsgstr);
    sgv = textscan(fsgstr, '%s', 'Delimiter', '');

    for k = 1 : nt_pt
        fprintf('\n\ncluster %d\n', k);
        mpt = zeros(nt_sg, nt_wd);
        mpt(:,:) = tcore(k,:,:);
        mpt = sum(mpt, 2);
        sorted_mpt = unique(mpt(:));
        top_mpt = sorted_mpt(end-tnum+1 : end);
        mpt_cut = min(top_mpt(:));
        top_mpt_idx = find(mpt >= mpt_cut);
        for i = 1 : size(top_mpt_idx,1)
            l = top_mpt_idx(i);
            fprintf('topic %d\n', l);
            for j = 1 : min(size(Rep,1), sgnum)
                if Rep(j,l) > 0 % in case bucket number > topic size
                    fprintf('%.2e: %s\n', Sol(j,l), sgv{1}{Rep(j,l)});
                end
            end
            fprintf('\n');
        end
    end

