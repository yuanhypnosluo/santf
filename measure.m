% pred and label are 1-d vector indicates the clustering results. 
function [precision, recall, F1, specificity] = measure(pred, label)
n = size(pred,1);
pair_pred = zeros(n*(n-1),1);
pair_label = zeros(n*(n-1),1);
for i = 1 : n
    if i ==1 
        h = 2 : n;
    else
        h = [1:i-1,i+1:n];
    end
    pair_pred( (i-1) * (n-1) + 1 : i *(n-1)) = (pred(h') == pred(i,1));
    pair_label( (i-1) *(n-1) + 1 : i * (n-1)) = (label(h') == label(i,1));
end
tp = sum(pair_pred.*pair_label);
fp = sum(pair_pred.*(~pair_label));
fn = sum((~pair_pred).*(pair_label));
tn = sum((~pair_pred).*(~pair_label));
precision = tp/(tp + fp);
recall = tp/(tp + fn);
specificity = fp/(fp+tn);
F1 = 2 * precision * recall / (precision + recall);

