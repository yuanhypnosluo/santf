function res = ntd_eval_doc_clustering(fngt, A, tcore, filter)

gt = load(fngt); 

doc_gt = full(spconvert(gt));

% for Tucker tensor, need to reweight by core.
nt_doc = size(tcore,1);
nt_w = size(tcore,3);
nt_sg = size(tcore,2);
weight = zeros(1,nt_doc);
dt = A{1};

for i = 1:nt_doc
    weight(1,i) = norm(tcore(i,:,:));
end

wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;
nsdm = dt/sum(dt(:));

% filter out multi-labels
doc_gt_tcnt = sum(doc_gt,2);
doc_gt = doc_gt(doc_gt_tcnt==1 & filter,:);
nsdm = nsdm(doc_gt_tcnt==1 & filter,:);

gt_1c = zeros(size(doc_gt,1),1);
for i = 1:nt_doc
    gt_1c(doc_gt(:,i)==1) = i;
end

labs = 1:nt_doc;


fprintf('\nSparsity on nsdm (%d x %d): %f\n', size(nsdm, 1), size(nsdm, 2), length(find(nsdm==0))/(size(nsdm,1)*size(nsdm,2)));

fprintf('\nSparsity on doc_gt (%d x %d): %f\n', size(doc_gt, 1), size(doc_gt, 2), length(find(doc_gt==0))/(size(doc_gt,1)*size(doc_gt,2)));



% treat the nsdm as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);
acc = zeros(psize, 1); mapre = acc; marec = acc; maf = acc; mipre = acc; mirec = acc; mif = acc;
for i = 1 : psize
    lab_perm = lab_perms(i, :);
    nsdm_perm = nsdm(:,lab_perm);
    [acc(i), mapre(i), marec(i), maf(i), mipre(i), mirec(i), mif(i) ] = clustering_accuracy(gt_1c, nsdm_perm);
end

[max_acc, idx] = max(acc);
max_lab_perm = lab_perms(idx,:);

fprintf('[doc_gt res] (%d, %d, %d) opt lab perm: [%d', nt_doc, nt_sg, nt_w, max_lab_perm(1));
for i = 2:size(max_lab_perm(:))
    fprintf(' %d', max_lab_perm(i));
end
fprintf(']; opt acc: %.3f, mapre: %.3f, marec: %.3f, maf: %.3f, mipre: %.3f, mirec: %.3f, mif: %.3f\n', max_acc, mapre(idx), marec(idx), maf(idx), mipre(idx), mirec(idx), mif(idx));


res = struct('opt_perm', max_lab_perm, 'opt_acc', max_acc, 'opt_mapre', mapre(idx), 'opt_marec', marec(idx), 'opt_maf', maf(idx), 'opt_mipre', mipre(idx), 'opt_mirec', mirec(idx), 'opt_mif', mif(idx));