function [] = cfs_disp(cfs, pts)
    fprintf('mis-clustered instances\n');
    for i = 1:size(cfs,1)
        gtc = cfs(i,1); mc = cfs(i,2); ptidx = cfs(i,3);
        fprintf('%d, %d, %s\n', gtc, mc, pts{ptidx});
    end
    