function res = nnmf_eval_pt_kl(fngt, nsdm)

gt = load(fngt); 
[npt, nt_pt] = size(nsdm);
labs = 1:nt_pt;
pt_gt = full(spconvert(gt));


fprintf('\nSparsity on nsdm (%d x %d): %f\n', size(nsdm, 1), size(nsdm, 2), length(find(nsdm==0))/(size(nsdm,1)*size(nsdm,2)));

fprintf('\nSparsity on pt_gt (%d x %d): %f\n', size(pt_gt, 1), size(pt_gt, 2), length(find(pt_gt==0))/(size(pt_gt,1)*size(pt_gt,2)));



% treat the nsdm as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);

kls = zeros(npt, psize);
for i = 1 : psize
    lab_perm = lab_perms(i, :);
    nsdm_perm = nsdm(:,lab_perm);
    
    for j = 1 : npt   
        if (max(nsdm_perm(j,:)) == 0)
            nsdm_perm(j,:) = nsdm_perm(j,:) + 1;
        end
        kls(j,i) = js_divergence(pt_gt(j,:), nsdm_perm(j,:));
        if isinf(kls(j,i))
            fprintf('kl inf!\n');
        end
    end
end
mean_kls = mean(kls, 1);
[mean_kl, idx] = min(mean_kls);
opt_lab_perm = lab_perms(idx,:);
kl = kls(:,idx);
std_kl = std(kl);
min_kl = min(kl);
max_kl = max(kl);

fprintf('\n[pt_gt res] opt lab perm: [%d', opt_lab_perm(1));
for i = 2:size(opt_lab_perm(:))
    fprintf(' %d', opt_lab_perm(i));
end
fprintf(']; mean_kl: %.3f, std_kl: %.3f, min_kl: %.3f, max_kl: %.3f\n', mean_kl, std_kl, min_kl, max_kl);

res = struct('mean_kl', mean_kl, 'kl', kl, 'std_kl', std_kl, 'min_kl', min_kl, 'max_kl', max_kl);