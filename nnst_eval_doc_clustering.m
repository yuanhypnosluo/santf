function res = nnst_eval_doc_clustering(fngt, T, ratio)

gt = load(fngt); 

doc_gt = full(spconvert(gt));

% for Tucker tensor, need to reweight by core.
tcore = T.core;
nt_doc = size(tcore,1);
nt_w = size(tcore,3);
nt_sg = size(tcore,2);
weight = zeros(1,nt_doc);
dt = T.U{1};

for i = 1:nt_doc
    weight(1,i) = norm(tcore(i,:,:));
end

wm = repmat(weight, size(dt,1), 1);
dt = dt.*wm;
nsdm = dt/sum(dt(:));


labs = 1:nt_doc;


% filter out multi-labels
doc_gt_tcnt = sum(doc_gt,2);
nsdm_rsum = sum(nsdm,2);
doc_gt = doc_gt(doc_gt_tcnt==1 & nsdm_rsum,:);
nsdm = nsdm(doc_gt_tcnt==1 & nsdm_rsum,:);

fprintf('\nSparsity on nsdm (%d x %d): %f\n', size(nsdm, 1), size(nsdm, 2), length(find(nsdm==0))/(size(nsdm,1)*size(nsdm,2)));

fprintf('\nSparsity on doc_gt (%d x %d): %f\n', size(doc_gt, 1), size(doc_gt, 2), length(find(doc_gt==0))/(size(doc_gt,1)*size(doc_gt,2)));



% treat the nsdm as the discovered topic distribution
lab_perms = perms(labs);
psize = size(lab_perms, 1);
acc = zeros(psize, 1);
for i = 1 : psize
    lab_perm = lab_perms(i, :);
    nsdm_perm = nsdm(:,lab_perm);
    acc(i) = clustering_accuracy(doc_gt, nsdm_perm);
end

[max_acc, idx] = max(acc);
max_lab_perm = lab_perms(idx,:);

fprintf('[doc_gt res] (%d, %d, %d, %f) opt lab perm: [%d', nt_doc, nt_sg, nt_w, ratio, max_lab_perm(1));
for i = 2:size(max_lab_perm(:))
    fprintf(' %d', max_lab_perm(i));
end
fprintf(']; opt clustering accuracy: %f\n', max_acc);



res = struct('opt_perm', max_lab_perm, 'opt_acc', max_acc);