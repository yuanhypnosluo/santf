%pred and label are 1-d vector indicates the clustering results. 
function [hmo] = cluster_align(pred, label)
tclusters = unique(label);
pclusters = unique(pred);

match = zeros(length(tclusters),length(pclusters));
tsize = length(tclusters);
psize = length(pclusters);

for i = 1 : tsize
    for j = 1 : psize
	      match(i,j) = length( intersect( find(label==tclusters(i)), find(pred==pclusters(j)) ) )/sum(label == tclusters(i));
    end
end
    ma = match;
msize = min(tsize, psize);

treorder = zeros(tsize,1);
preorder = zeros(psize,1);
for k = 1 : msize
    if k == msize
	  sum(sum(match~=0))
end
    [maxc, ind] = max(match(:));
    if maxc > 0
        [i,j] = ind2sub(size(match), ind);
        match(i,:) = 0; match(:,j) = 0;
        treorder(k) = i; preorder(k) = j;
    else
        msize=k-1;
        break
    end
end
	match = zeros(msize, msize);
msize
treorder
preorder
for i = 1 : msize
    for j = 1 : msize
        %match(i,j) = length( intersect( find(label==treorder(i)), find(pred==preorder(j)) ) );
        match(i,j) = ma(treorder(i), preorder(j));
    end
end
    B = match;
for i = 1 : msize
	  B(i,:) = match(msize+1-i,:);
end

hmo = HeatMap(B);
