function dist = sg_wd_topic_dist(fnA, fnC, sgid, sg)
% wd, the word you want to find the distribution
    addpath('~/Code/tensor/tensor_toolbox');
    addpath('~/Code/poblano/poblano_toolbox');
    
    astruct = load(fnA);
    A = astruct.A;
    
    cstruct = load(fnC);
    tcore = cstruct.C;
    ntpt = size(tcore, 1);
    ntwd = size(tcore, 3);
    
    
    sg_sgt = A{2};
    sgv = sg_sgt(sgid,:);
    ptt_wdt = zeros(ntpt, ntwd);
    
    for i = 1:size(tcore,2)
        ptt_wdt(:,:) = ptt_wdt(:,:) + sgv(i) * tcore(:,i,:);
    end
    
    pt_ptt = A{1};
    % normalize within each topic
    tm = sum(pt_ptt * ptt_wdt, 1);
    
    % fprintf('%s id is %d\n', wd, wdid);
    figure;
    dist = tm/sum(tm(:));
    bar(dist);
    set(gca, 'XLim', [1, length(tm)]);
    title(sprintf('%s word group dist', sg));