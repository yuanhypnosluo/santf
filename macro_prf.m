function [mapre, marec, maf] = macro_prf(actual, pred, max_class)
% calculate precision, recall and f-measure given result output
mapre = 0; marec = 0; maf = 0;
for class = 1 : max_class
    tp = size(find(actual==class & pred==class), 1);
    fp = size(find(actual~=class & pred==class), 1);
    tn = size(find(actual~=class & pred~=class), 1);
    fn = size(find(actual==class & pred~=class), 1);
    if(tp+fp == 0) pre = 1; else pre = tp / (tp+fp); end
    if(tp+fn == 0) rec = 1; else rec = tp / (tp+fn); end
    if(pre+rec == 0) f = 0; else f = 2*pre*rec / (pre+rec); end
    mapre = pre + mapre;
    marec = rec + marec;
    maf = f + maf;
end

mapre = mapre / max_class;
marec = marec / max_class;
maf = maf / max_class;
end