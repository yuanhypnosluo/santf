function res = nnmf_eval_sg(fngt, nsdm_orig)

gt = load(fngt); 
nt_sg = size(nsdm_orig,2);
labs = 1:nt_sg;
sg_gt = full(spconvert(gt));
% sg_gt = sg_gt';
% sg_gt = row_norm(sg_gt);
sg_gt = col_norm(sg_gt);

fprintf('\nSparsity on sg_gt (%d x %d): %f\n', size(sg_gt, 1), size(sg_gt, 2), length(find(sg_gt==0))/(size(sg_gt,1)*size(sg_gt,2)));

smooths = 10.^(-7:1:1);
opt_perms = zeros(length(smooths), nt_sg);
opt_kl_divs = zeros(length(smooths), 1);

for iter = 1 : length(smooths)
    smooth = smooths(iter);
    nsdm = nsdm_orig;
    nsdm = col_smooth_norm(nsdm, smooth); % normalize distributions across columns (topics)
    
    % treat the nsdm as the discovered topic distribution
    lab_perms = perms(labs);
    psize = size(lab_perms, 1);
    kl_div = zeros(psize, 1);
    for i = 1 : psize
        lab_perm = lab_perms(i, :);
        nsdm_perm = nsdm(:,lab_perm);
        kl_div(i) = kl_divergence(sg_gt, nsdm_perm) / nt_sg;
    end
    
    [min_kl_div, idx] = min(kl_div);
    min_lab_perm = lab_perms(idx,:);
    
    fprintf('[sg_gt res] %.1e opt lab perm: [%d', smooth, min_lab_perm(1));
    for i = 2:size(min_lab_perm(:))
        fprintf(' %d', min_lab_perm(i));
    end
    fprintf(']; opt kl divergence: %f\n', min_kl_div);
    opt_perms(iter,:) = min_lab_perm;
    opt_kl_divs(iter) = min_kl_div;
end

res = struct('smooths', smooths, 'opt_perms', opt_perms, 'opt_kl_divs', opt_kl_divs);